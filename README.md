For this project, test.py has an inline comment to ignore a finding based on a rule.

The rule should be bandit.B501 but I've set an incorrect rule bandit.B502.

The job exits with a status code of 2.

gl-sast-report.json is not produced but from the job log, the sarif output has this entry:

[{"descriptor": {"id": "SemgrepError"}, "level": "warning", "message": {"text": "found 'nosem' comment with id 'bandit.B502', but no corresponding rule trying 'bandit.B501'"}}]}]
